package id.co.ptdmc.modabile.handlers;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class ContentHandler implements HttpHandler {

	private String contentValue;
	private HttpHandler next;

	public ContentHandler(String contentValue, HttpHandler next) {
		super();
		this.contentValue = contentValue;
		this.next = next;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		exchange.getResponseSender().send(contentValue);
		if (next != null) {
			next.handleRequest(exchange);
		}
	}

}
