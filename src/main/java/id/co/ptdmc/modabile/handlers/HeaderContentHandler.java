package id.co.ptdmc.modabile.handlers;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

public class HeaderContentHandler implements HttpHandler {

	private HttpString header;
	private String headerValue;
	private String contentValue;

	public HeaderContentHandler(String header, String headerValue,
			String contentValue) {
		super();
		this.header = new HttpString(header);
		this.headerValue = headerValue;
		this.contentValue = contentValue;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		exchange.getResponseHeaders().put(header, headerValue);
		exchange.getResponseSender().send(contentValue);
	}

}
