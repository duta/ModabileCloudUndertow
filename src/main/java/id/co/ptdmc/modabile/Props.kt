package id.co.ptdmc.modabile

import java.io.*
import java.util.Properties

/**
 * Created by root on 09/03/16.
 */
public object Props  {
    val prop = Properties()
    init {
        try {
            val fileName : String  = "config.properties"
            val istream : InputStream  = FileInputStream(fileName)
            prop.load(istream)
        } catch (ex : IOException ) {
            ex.printStackTrace()
        }
    }
}