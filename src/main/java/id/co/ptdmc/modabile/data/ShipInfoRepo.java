package id.co.ptdmc.modabile.data;

import id.co.ptdmc.modabile.data.entities.ShipInfo;
import org.sql2o.Connection;

import java.util.List;

/**
 * Created by root on 09/03/16.
 */
public class ShipInfoRepo {
    public ShipInfo GetByMMSI(int mmsi){
        ShipInfo result = new ShipInfo();
        StringBuilder sb = new StringBuilder();
        sb.append("select received_on, mmsi from ship_info where mmsi =  :mmsi");
        Connection conn = DBProvider.INSTANCE.getSql2o().open();
        List<ShipInfo> ships = conn.createQuery(sb.toString())
                .addParameter("mmsi",mmsi)
                .executeAndFetch(ShipInfo.class);
        if (ships.size() > 0) {
            result = ships.get(0);
        }
        return result;
    }
}
