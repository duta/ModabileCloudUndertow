package id.co.ptdmc.modabile.data

import id.co.ptdmc.modabile.*
import org.sql2o.Sql2o
import java.util.Properties

/**
 * Created by root on 09/03/16.
 */
public object DBProvider {
    val sql2o : Sql2o

    init {
        val prop : Properties = Props.prop
        sql2o = Sql2o(prop.getProperty("pgconnstring"),prop.getProperty("pguser"),prop.getProperty("pgpassword"))
    }
}