package id.co.ptdmc.modabile.servlets

import org.apache.log4j.Logger
import java.io.IOException

import javax.servlet.ServletException
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by root on 09/03/16.
 */
class HelloKotlinServlet : HttpServlet() {
//    internal val logger = Logger.getLogger(HelloKotlinServlet::class.java)
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        try {
            val out = resp.writer
            out.println("<html>")
            out.println("<body>")
            out.println("<h1>Hello Kotlin Servlet</h1>")
            out.println("</body>")
            out.println("</html>")
        }
        catch (se : ServletException){
        }
        catch (ioe : IOException){
        }


    }
}