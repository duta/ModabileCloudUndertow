package id.co.ptdmc.modabile.servlets;

import com.google.gson.Gson;
import id.co.ptdmc.modabile.data.ShipInfoRepo;
import id.co.ptdmc.modabile.data.entities.ShipInfo;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by root on 09/03/16.
 */
public class ShipInfoServlet extends HttpServlet {
    final static Logger logger = Logger.getLogger(ShipInfoServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res){
        try {
            Integer mmsi = Integer.parseInt(req.getPathInfo().replace("/",""));
            ShipInfoRepo shipInfoRepo = new ShipInfoRepo();
            ShipInfo shipInfo =  shipInfoRepo.GetByMMSI(mmsi);
            String json = new Gson().toJson(shipInfo);
            res.setContentType("application/json");
            res.setCharacterEncoding("UTF-8");
            res.getWriter().write(json);
        }
        catch (Exception ex){
            logger.error(ex.getMessage(),ex);
        }
    }
}
